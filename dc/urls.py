from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django_dacs.views import login_redirect
from contributors import views as cviews

from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
    url(r'^$', cviews.contributors, name="contributors"),

    # Examples:
    # url(r'^$', 'dc.views.home', name='home'),
    # url(r'^dc/', include('dc.foo.urls')),

    url(r'^license/$', TemplateView.as_view(template_name='license.html'), name="root_license"),
    url(r'^login/dacs$', login_redirect, name="login_redirect"),

    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^contributors/', include('contributors.urls')),
    url(r'^sources/', include('sources.urls')),
)
