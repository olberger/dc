Debian Contributors web application
===================================

See https://wiki.debian.org/Teams/FrontDesk/DcSiteDevel for this project's page in the Debian wiki.

## Running this code on your own machine

### Dependencies
    
    apt-get install python-django python-django-south

### Configuration

    cp dc/local_settings.py.devel dc/local_settings.py
    edit dc/local_settings.py as needed

### First setup

    ./manage.py syncdb
    ./manage.py migrate

You may wish to add an admin user that will be named the same as the DACS_TEST_USERNAME (see Authentication below).

### Fill in data

Import source information from http://contributors.debian.org/contributors/export/sources
if you are not logged as a DD, authentication tokens are replaced with dummy
ones:

    curl https://contributors.debian.org/contributors/export/sources | ./manage.py import_sources

    eatmydata ./manage.py runserver

From now on, imported sources will be available from http://localhost:8000/sources/. 
You may login to http://localhost:8000/admin/ which grants access to the sources and other tables.

Acquire some JSON data (ask Enrico at the moment) and import it:

    ./dc-post-source --baseurl=http://localhost:8000 sourcename sourcetoken datafile.json

So, for instance, if you're a DD, fetch the website contribs data with :

    scp git.debian.org:~enrico/www-dc.json .

And import it with (the token is available from the admin interface if you configured the source accordingly) :
    
	./dc-post-source --baseurl=http://localhost:8000 www.debian.org whatevertokenisneeded www-dc.json

### Run database maintenance

Update the alioth and debian UIDs, and create Django user accounts accordingly :

    ./update-data
    eatmydata ./manage.py maintenance

### Run the web server
    
    ./manage.py runserver

## Development

Development currently targets Django 1.5.

You can find a TODO list at https://wiki.debian.org/Teams/FrontDesk/DcSiteDevel

## Authentication

Authentication in production is meant to be performed against DACS, the Debian SSO system.
The django_dacs/ authenticator generates Django authentication from the REMOTE_USER 
variable retrieved from DACS.
For testing purposes, when not on a debian.org machine allowed to query sso.debian.org, 
one may set the DACS_TEST_USERNAME config to specify which user name to use.

