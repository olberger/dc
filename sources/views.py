# Debian Contributors source management functions
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Create your views here.

from __future__ import absolute_import
from django import http, template, forms
from django.shortcuts import redirect, render, get_object_or_404
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse, reverse_lazy
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.forms import ModelForm
import contributors.models as cmodels

def sources_list(request):
    return render(request, "sources/list.html", {
        "sources": cmodels.Source.objects.all().order_by("name"),
    })

def source_view(request, name):
    source = get_object_or_404(cmodels.Source, name=name)
    # TODO: filter out connections hidden by settings
    return render(request, "sources/view.html", {
        "source": source,
        "ctypes": source.contribution_types.all().order_by("desc"),
        "contribs": cmodels.Contribution.objects.filter(
            type__source=source,
            identifier__user__isnull=False
        ).order_by("type__desc", "identifier__user__username"),
    })

def ctype_view(request, name, cname):
    source = get_object_or_404(cmodels.Source, name=name)
    ct = get_object_or_404(cmodels.ContributionType, source=source, name=cname)
    # TODO: filter out connections hidden by settings
    return render(request, "sources/cview.html", {
        "source": source,
        "ct": ct,
        "contribs": cmodels.Contribution.objects.filter(
            type=ct,
            identifier__user__isnull=False
        ).order_by("type__desc", "identifier__user__username"),
    })

class SourceForm(ModelForm):
    class Meta:
        model = cmodels.Source
        exclude = ("admins",)

class SourceMixin(object):
    model = cmodels.Source
    slug_field = "name"
    slug_url_kwarg = "name"
    form_class = SourceForm
    success_url = reverse_lazy('sources_list')

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(SourceMixin, self).dispatch(*args, **kwargs)

class SourceCreate(SourceMixin, CreateView):
    template_name = "sources/source_form.html"

class SourceUpdate(SourceMixin, UpdateView):
    template_name = "sources/source_form.html"

class SourceDelete(SourceMixin, DeleteView):
    template_name = "sources/source_confirm_delete.html"


class ContributionTypeForm(ModelForm):
    class Meta:
        model = cmodels.ContributionType
        exclude = ('source',)

class ContributionTypeMixin(object):
    model = cmodels.ContributionType
    slug_field = "name"
    slug_url_kwarg = "name"
    form_class = ContributionTypeForm

    def get_queryset(self):
        res = super(ContributionTypeMixin, self).get_queryset()
        source = get_object_or_404(cmodels.Source, name=self.kwargs["sname"])
        return res.filter(source=source)

    def get_success_url(self):
        obj = getattr(self, "object", None)
        if obj is not None:
            return reverse('sources_update', kwargs={"name": obj.source.name})
        else:
            return reverse('sources_update', kwargs={"name": self.kwargs["sname"]})

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ContributionTypeMixin, self).dispatch(*args, **kwargs)

class ContributionTypeCreate(ContributionTypeMixin, CreateView):
    template_name = "sources/contributiontype_form.html"

    def form_valid(self, form):
        source = get_object_or_404(cmodels.Source, name=self.kwargs["sname"])
        form.instance.source = source
        return super(ContributionTypeCreate, self).form_valid(form)

class ContributionTypeUpdate(ContributionTypeMixin, UpdateView):
    template_name = "sources/contributiontype_form.html"

class ContributionTypeDelete(ContributionTypeMixin, DeleteView):
    template_name = "sources/source_confirm_delete.html"
