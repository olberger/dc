# Debian Contributors source management functions
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import *
from django.views.generic import TemplateView, RedirectView
from sources.views import SourceCreate, SourceUpdate, SourceDelete, ContributionTypeCreate, ContributionTypeUpdate, ContributionTypeDelete

urlpatterns = patterns('sources.views',
    url(r'^$', "sources_list", name="sources_list"),
    url(r'^view/(?P<name>[^/]+)/$', "source_view", name='source_view'),
    url(r'^view/(?P<name>[^/]+)/(?P<cname>[^/]+)/$', "ctype_view", name='source_ctype_view'),
    url(r'^add/$', SourceCreate.as_view(), name='sources_add'),
    url(r'^update/(?P<name>[^/]+)/$', SourceUpdate.as_view(), name='sources_update'),
    url(r'^delete/(?P<name>[^/]+)/$', SourceDelete.as_view(), name='sources_delete'),
    url(r'^(?P<sname>[^/]+)/ctype/add/$', ContributionTypeCreate.as_view(), name='sources_ctype_add'),
    url(r'^(?P<sname>[^/]+)/ctype/update/(?P<name>[^/]+)/$', ContributionTypeUpdate.as_view(), name='sources_ctype_update'),
    url(r'^(?P<sname>[^/]+)/ctype/delete/(?P<name>[^/]+)/$', ContributionTypeDelete.as_view(), name='sources_ctype_delete'),
    #url(r'^newnm$', TemplateView.as_view(template_name='public/newnm.html'), name="public_newnm"),
    #url(r'^processes$', 'processes', name="processes"),
    #url(r'^managers$', 'managers', name="managers"),
    #url(r'^people(?:/(?P<status>\w+))?$', 'people', name="people"),
    #url(r'^person/(?P<key>[^/]+)$', 'person', name="person"),
    #url(r'^process/(?P<key>[^/]+)$', 'process', name="public_process"),
    #url(r'^progress/(?P<progress>\w+)$', 'progress', name="public_progress"),
    #url(r'^stats/$', 'stats', name="public_stats"),
    #url(r'^stats/latest$', 'stats_latest', name="public_stats_latest"),
    #url(r'^findperson/$', 'findperson', name="public_findperson"),
)
