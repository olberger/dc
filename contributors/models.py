# Debian Contributors website backend
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth.models import User
import datetime


class UserProfile(models.Model):
    """
    Extra information about a user
    """
    # Link to Django web user information
    user = models.OneToOneField(User)
    # TODO: other fields as needed, like settings, short bio

    def __unicode__(self):
        return self.user.get_full_name()


class Identifier(models.Model):
    """
    One of the many ways identity is tracked in various bits of Debian.

    For example, debian login, alioth login, gpg key fingerprint, email
    address, wiki name.
    """
    # Type of identifier
    type = models.CharField(max_length=16, choices=(
        ("login", _("Debian/Alioth login")),
        ("fpr", _("OpenPGP key fingerprint")),
        ("email", _("Email address")),
        ("wiki", _("Wiki name")),
    ))
    # Identity name
    name = models.CharField(max_length=256)
    # If the owner of this identifier registed to log in with the contributor
    # site, we have a link to Django's User here.
    user = models.ForeignKey(User, null=True, related_name="identifiers")
    # True if the person does not want activity for this identifier to be shown
    hidden = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name

    class Meta:
        unique_together = ('type', 'name')


class Source(models.Model):
    """
    A data source, providing contribution informations.
    """
    # Machine-readable source name
    name = models.CharField(max_length=32, unique=True)
    # Human readable source description
    desc = models.TextField()
    # Optional URL to a page about the source
    url = models.URLField(null=True, blank=True, max_length=255)
    # Authentication token used by the source when POSTing data
    auth_token = models.CharField(max_length=255)
    # Implementation notes shown when configuring the source
    implementation_notes = models.TextField(blank=True)
    # People who maintain this Source
    admins = models.ManyToManyField(User)

    def __unicode__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ("source_view", (), {"name": self.name})

    @classmethod
    def import_json(cls, data):
        from .importer import SourceImporter
        si = SourceImporter()
        si.import_sources(data)

    @classmethod
    def export_json(cls, names=None, with_tokens=False):
        """
        Export Source and ContributionType information into a JSON-able array
        """
        if names:
            sources = cls.objects.find(name__in=names)
        else:
            sources = cls.objects.all()

        fake_tok = 1
        res = []
        for s in sources:
            if with_tokens:
                tok = s.auth_token
            else:
                tok = "token_%d" % fake_tok
                fake_tok += 1

            ctypes = []

            js = {
                "name": s.name,
                "desc": s.desc,
                "url": s.url,
                "auth_token": tok,
                "contribution_types": ctypes,
            }

            for ct in s.contribution_types.all():
                jct = {
                    "name": ct.name,
                    "desc": ct.desc,
                    "contrib_desc": ct.contrib_desc,
                }
                ctypes.append(jct)

            res.append(js)

        return res


class ContributionType(models.Model):
    """
    A contribution type for a data source
    """
    # Data source that manages this contribution type
    source = models.ForeignKey(Source, related_name="contribution_types")
    # Machine readable contribution type
    name = models.CharField(max_length=32)
    # Human readable contribution type description
    desc = models.TextField()
    # Description for the kind of contribution
    contrib_desc = models.TextField()

    def __unicode__(self):
        return "%s:%s" % (self.source.name, self.name)

    @models.permalink
    def get_absolute_url(self):
        return ("source_ctype_view", (), {"name": self.source.name, "cname": self.name})

    class Meta:
        unique_together = ('source', 'name')


class ContributionTypeSettings(models.Model):
    """
    User settings for a contribution type
    """
    # Identifier for which we are holding settings
    identifier = models.ForeignKey(Identifier, related_name="type_settings")
    # Contribution type the person wants to control
    type = models.ForeignKey(ContributionType, related_name="identifier_settings")
    # Do not show this kind of contributions for this identifier
    hidden = models.BooleanField()

    class Meta:
        unique_together = ('identifier', 'type')


class Contribution(models.Model):
    """
    Description of contributions by one identifier in a source+type
    """
    # Type of contribution
    type = models.ForeignKey(ContributionType, related_name="contributions")
    # Identifier used for this contribution
    identifier = models.ForeignKey(Identifier, related_name="contributions")
    # When the first contribution was found
    begin = models.DateField()
    # When the last contribution was found
    until = models.DateField()
    # Optional pointer to some team-specific info page
    url = models.URLField(null=True, blank=True, max_length=255)

    class Meta:
        unique_together = ('identifier', 'type')

    def save(self, *args, **kw):
        """
        Make sure we always fill begin and until, defaulting to today
        """
        if self.begin is None:
            self.begin = datetime.date.today()
        if self.until is None:
            self.until = datetime.date.today()
        super(Contribution, self).save(*args, **kw)

    def extend(self, begin, until):
        """
        Extend the begin-until date range with a new date range.

        If begin is missing, defaults to the previous begin value.
        If no begin value was ever set, default to today.

        If until is missing, default to today.

        Returns True if the date range was changed, else False.
        """
        changed = False

        # If missing, defaults to previous 'begin' value.
        if begin is None:
            begin = self.begin
        # If never seen, defaults to 'today'
        if begin is None:
            # If a data source only reports 'until', we have a better default
            # than 'today'
            if until is not None:
                begin = until
            else:
                begin = datetime.date.today()
        if self.begin != begin:
            self.begin = begin
            changed = True

        # If missing, defaults to 'today'
        if until is None:
            until = datetime.date.today()
        if self.until != until:
            self.until = until
            changed = True

        return changed
