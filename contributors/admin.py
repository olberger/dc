from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
import contributors.models as cmodels

# Define an inline admin descriptor for UserProfile model
# which acts a bit like a singleton
class UserProfileInline(admin.StackedInline):
    model = cmodels.UserProfile
    can_delete = False

# Define a new User admin
class UserAdmin(UserAdmin):
    inlines = (UserProfileInline, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

class IdentifierAdmin(admin.ModelAdmin):
    list_display = ('name', 'type')
    #search_fields = ("person__cn", "person__sn", "person__email", "person__uid")
    pass
admin.site.register(cmodels.Identifier, IdentifierAdmin)

class SourceAdmin(admin.ModelAdmin):
    pass
admin.site.register(cmodels.Source, SourceAdmin)

class ContributionTypeAdmin(admin.ModelAdmin):
    pass
admin.site.register(cmodels.ContributionType, ContributionTypeAdmin)

class ContributionTypeSettingsAdmin(admin.ModelAdmin):
    pass
admin.site.register(cmodels.ContributionTypeSettings, ContributionTypeSettingsAdmin)

class ContributionAdmin(admin.ModelAdmin):
    pass
admin.site.register(cmodels.Contribution, ContributionAdmin)
