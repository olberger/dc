# Debian Contributors import/export
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf.urls import patterns, url
from django.views.generic import DetailView
from django.contrib.auth.models import User
from contributors.models import Source

urlpatterns = patterns('contributors.views',
    url(r'^post$', 'post_source', name="contributors_post"),
    url(r'^flat$', 'contributors_flat', name="contributors_flat"),
    url(r'^export/sources$', 'export_sources', name="contributors_export_sources"),
    url(r'^contributor/(?P<name>[^/]+)',
        DetailView.as_view(model=User, slug_field='username',
                           slug_url_kwarg='name',
                           template_name="contributors/contributor.html",
                           context_object_name='contributor'),
        name='contributor_detail'),

    url(r'^source/(?P<sname>[^/]+)',
        DetailView.as_view(model=Source, slug_field='name', slug_url_kwarg='sname',
                           template_name="contributors/source.html",
                           context_object_name='source'),
        name='source_detail'),
)
