# Debian Contributors website backend
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand
import optparse
import sys
import logging
import json
from contributors import randomsource

log = logging.getLogger(__name__)

class Command(BaseCommand):
    help = 'Generate a random data source submission'
    option_list = BaseCommand.option_list + (
        optparse.make_option("--types", action="store", dest="types", default="upload,sponsor", metavar="string",
                             help="Types to use to generate random contributions (default: %default)"),
        optparse.make_option("--quiet", action="store_true", dest="quiet", default=None, help="Disable progress reporting"),
    )

    def handle(self, *fnames, **opts):
        FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
        if opts["quiet"]:
            logging.basicConfig(level=logging.WARNING, stream=sys.stderr, format=FORMAT)
        else:
            logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)

        types = opts["types"].split(",")

        gen = randomsource.SourceGenerator(types)

        contributions = []
        for i in xrange(10):
            c = gen.make_contributions()
            contributions.append(c)

        json.dump(contributions, sys.stdout, indent=2)
