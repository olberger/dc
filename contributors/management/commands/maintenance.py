# Debian Contributors website backend
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from django.db import models
from contributors import models as cmodels
import optparse
import sys
import logging
import json

log = logging.getLogger(__name__)

class Maintenance(object):
    """Maintains User accounts on the application for Debian logins  and alioth -guest logins"""
    def __init__(self):
        with open("data/debian-uids") as fd:
            self.debian_uids = { x.strip() for x in fd }
        with open("data/alioth-uids") as fd:
            self.alioth_uids = { x.strip() for x in fd }

        self.assoc_users_reused = 0
        self.assoc_users_created = 0
        self.assoc_associated = 0

    def _associate_identity(self, identity, uid):
        try:
            identity.user = User.objects.get(username=uid)
            self.assoc_users_reused += 1
        except User.DoesNotExist:
            # Infer an email address
            if uid.endswith("-guest"):
                email = uid + "@users.alioth.debian.org"
            else:
                email = uid + "@debian.org"
            identity.user = User.objects.create_user(uid, email)
            self.assoc_users_created += 1
        identity.save()
        self.assoc_associated += 1

    def default_associations(self):
        """
        Automatically make some default, safe associations
        """
        for i in cmodels.Identifier.objects.filter(type="login", user__isnull=True):
            # Ensure we only work on valid uids
            if i.name not in self.debian_uids and i.name not in self.alioth_uids:
                continue

            # Associate the user
            self._associate_identity(i, i.name)

        for i in cmodels.Identifier.objects.filter(type="email", user__isnull=True):
            # Infer a valid login name from the email addresses
            if i.name.endswith("@debian.org"):
                uid = i.name[:-11]
                if uid not in self.debian_uids: continue
            elif i.name.endswith("@users.alioth.debian.org"):
                uid = i.name[:-24]
                if uid not in self.alioth_uids: continue
            else:
                continue

            # Associate the user
            self._associate_identity(i, uid)


class Command(BaseCommand):
    help = 'Import data source information'
    option_list = BaseCommand.option_list + (
        optparse.make_option("--quiet", action="store_true", dest="quiet", default=None, help="Disable progress reporting"),
    )

    def handle(self, quiet=False, *args, **opts):
        FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
        if quiet:
            logging.basicConfig(level=logging.WARNING, stream=sys.stderr, format=FORMAT)
        else:
            logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)

        maint = Maintenance()
        maint.default_associations()

        log.info("Associations.users_reused: %d", maint.assoc_users_reused)
        log.info("Associations.users_created: %d", maint.assoc_users_created)
        log.info("Associations.users_associated: %d", maint.assoc_associated)
