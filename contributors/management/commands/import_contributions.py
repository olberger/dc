# Debian Contributors website backend
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.management.base import BaseCommand, CommandError
from contributors import models as bmodels
from contributors import importer
import optparse
import sys
import logging
import json

log = logging.getLogger(__name__)

class Command(BaseCommand):
    help = 'Import a data source submission'
    option_list = BaseCommand.option_list + (
        optparse.make_option("--quiet", action="store_true", dest="quiet", default=None, help="Disable progress reporting"),
    )

    def handle(self, *args, **opts):
        FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
        if opts["quiet"]:
            logging.basicConfig(level=logging.WARNING, stream=sys.stderr, format=FORMAT)
        else:
            logging.basicConfig(level=logging.INFO, stream=sys.stderr, format=FORMAT)

        if not args:
            raise CommandError("Please specify the source name")
        args = list(args)

        name = args.pop(0)

        try:
            source = bmodels.Source.objects.get(name=name)
        except bmodels.Source.DoesNotExist:
            raise CommandError("Source %s does not exist" % name)

        if args:
            data = []
            for fname in args:
                with open(fname, "rt") as fd:
                    data.extend(json.load(fd))
        else:
            data = json.load(sys.stdin)

        i = importer.Importer(source)
        for contrs in data:
            i.import_data(contrs)
