# Debian Contributors import/export and basic website functions
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from django.utils.translation import ugettext as _
from django import http, template, forms
from django.shortcuts import redirect, render, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from contributors.importer import Importer
from django.contrib.auth.models import User
import contributors.models as cmodels
import datetime
import json

@csrf_exempt
def post_source(request):
    # Import the data
    importer = Importer()
    importer.import_request(request)
    return importer.results.to_response()

def export_sources(request):
    if request.user.is_anonymous():
        data = cmodels.Source.export_json()
    else:
        data = cmodels.Source.export_json(with_tokens=True)

    res = http.HttpResponse(content_type="application/json")
    json.dump(data, res, indent=2)
    return res

def contributors(request):
    from django.db.models import Max
    import itertools

    # Query all people
    people = User.objects.filter() \
            .annotate(until=Max("identifiers__contributions__until")) \
            .order_by("-until")

    today = datetime.date.today()
    def get_year(p):
        if p.until is None:
            return today.year
        else:
            return p.until.year

    people = itertools.groupby(people, get_year)

    return render(request, "contributors/contributors.html", {
        "people": ((k, list(v)) for k, v in people),
    })

def contributors_flat(request):
    from django.db.models import Min, Max

    # Query all people
    people = User.objects.filter() \
            .annotate(begin=Min("identifiers__contributions__begin")) \
            .annotate(until=Max("identifiers__contributions__until")) \
            .order_by("-until")

    return render(request, "contributors/contributors_flat.html", {
        "people": people
    })


def view(request, pid):
    return render(request, "contributors/view.html", ctx)
