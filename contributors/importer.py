# Debian Contributors website backend
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import absolute_import
from . import models
from django import http
import datetime
import re
import json
import logging

log = logging.getLogger(__name__)

def encode_untrusted(s):
    return s.encode("utf8").encode("string-escape")


class Fail(BaseException):
    """
    Exception raised when a validation or lookup fails
    """
    def __init__(self, results):
        self.results = results


class Results(object):
    def __init__(self):
        self.code = 200
        self.identifiers_skipped = 0
        self.contributions_processed = 0
        self.contributions_created = 0
        self.contributions_updated = 0
        self.contributions_skipped = 0
        self.records_processed = 0
        self.records_imported = 0
        self.records_skipped = 0
        self.errors = []

    def error(self, code, msg):
        self.code = code
        self.errors.append(msg)
        raise Fail(self)

    def to_response(self):
        res = {
            "code": self.code,
            "errors": self.errors,
            "identifiers_skipped": self.identifiers_skipped,
            "contributions_processed": self.contributions_processed,
            "contributions_created": self.contributions_created,
            "contributions_updated": self.contributions_updated,
            "contributions_skipped": self.contributions_skipped,
            "records_processed": self.records_processed,
            "records_imported": self.records_imported,
            "records_skipped": self.records_skipped,
        }
        response = http.HttpResponse(content_type="text/plain", status=self.code)
        json.dump(res, response, indent=2)
        return response

    def test_dump(self, out=None):
        import sys
        if out is None:
            out = sys.stdout

        print >>out, "Results.code:", self.code
        print >>out, "Results.identifiers_skipped:", self.identifiers_skipped
        print >>out, "Results.contributions_processed:", self.contributions_processed
        print >>out, "Results.contributions_created:", self.contributions_created
        print >>out, "Results.contributions_updated:", self.contributions_updated
        print >>out, "Results.contributions_skipped:", self.contributions_skipped
        print >>out, "Results.records_processed:", self.records_processed
        print >>out, "Results.records_imported:", self.records_imported
        print >>out, "Results.records_skipped:", self.records_skipped
        for e in self.errors:
            print >>out, "Results.errors:", e


class ValidatingDict(object):
    def __init__(self, results, data, name, strict_dict=True):
        self.results = results
        self.data = data
        self.name = name
        if strict_dict and not isinstance(data, dict):
            self.results.error(400, "%s is not a dictionary" % self.name)


    def get_any(self, key):
        try:
            return self.data[key]
        except KeyError:
            self.results.error(400, "Key '%s' not found in %s" % (key, self.name))

    def get_int(self, key):
        try:
            return int(self.get_any(key))
        except ValueError:
            self.results.error(400, "Key '%s' in %s does not contain an integer value" % (key, self.name))

    def get_string(self, key, empty=False):
        if empty:
            res = self.data.get(key, "")
            if not res: return ""
        else:
            res = self.get_any(key)
        try:
            res = str(res)
        except ValueError:
            self.results.error(400, "Key '%s' in %s does not contain a string value" % (key, self.name))
        if not empty and not res:
            self.results.error(400, "Key '%s' in %s contains an empty string" % (key, self.name))
        return res

    def get_unicode(self, key, empty=False):
        try:
            return self.get_string(key, empty).decode("utf8")
        except UnicodeDecodeError:
            self.results.error(400, "Key '%s' in %s does not contain a valid UTF8 string" % (key, self.name))

    def get_json(self, key):
        try:
            return json.loads(self.get_string(key))
        except (ValueError, UnicodeDecodeError):
            self.results.error(400, "Key '%s' in %s does not contain valid JSON" % (key, self.name))

    def get_sequence(self, key):
        res = self.get_any(key)
        if not isinstance(res, (list, tuple)):
            self.results.error(400, "Key '%s' in %s does not contain a list" % (key, self.name))
        return res

    def get_date_or_none(self, key):
        # Allow for a missing or null value
        res = self.get_string(key, empty=True)
        if not res: return None

        try:
            return datetime.datetime.strptime(res, "%Y-%m-%d").date()
        except ValueError:
            self.results.error(400, "Key '%s' in %s does not contain a YYYY-MM-DD date" % (key, self.name))


class ValidatingIdentitifierDict(ValidatingDict):
    def __init__(self, results, data, name, strict_dict=True):
        super(ValidatingIdentitifierDict, self).__init__(results, data, name, True)

        # Validator regexps
        self.validate_type_login = re.compile("^[a-z0-9.-]+$")
        # From http://www.regular-expressions.info/email.html
        self.validate_type_email = re.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$", re.I)
        self.validate_type_fpr = re.compile("^[A-F0-9]{32,40}$")
        self.validate_type_wiki = re.compile("^[A-Za-z]+$")

    def get_type(self, key):
        t = self.get_string(key)
        if not hasattr(self, "validate_type_" + t):
            self.results.error(400, "Key '%s' in %s does not contain a valid identifier type" % (key, self.name))
        return t

    def get_name(self, itype, key):
        validate_re = getattr(self, "validate_type_" + itype)
        res = self.get_unicode(key)
        if not validate_re.match(res):
            self.results.error(400, "Key '%s' in %s does not contain a valid identifier %s" % (key, self.name, itype))
        return res


class Source(object):
    """
    Source-specific data import infrastructure

    Methods starting with import_* return True if something was imported, else
    False.

    Other methods return what they should, or raise Fail if some validation failed.
    """

    def __init__(self, results, source):
        self.results = results
        self.source = source
        self.types = dict()

        # Preload contribution types
        for ct in models.ContributionType.objects.filter(source=source):
            self.types[ct.name] = ct
        if not self.types:
            log.warning("Source %s has no configured contribution types" % source.name)

    def validate_auth_token(self, auth_token):
        if self.source.auth_token != auth_token:
            self.results.error(403, "Authentication token is not correct")

    def get_identifier(self, data):
        """
        Create/retrieve an Identifier given a dict coming from JSON
        """
        data = ValidatingIdentitifierDict(self.results, data, "identity JSON data")

        id_type = data.get_type("type")
        name = data.get_name(id_type, "id")

        # Lookup an identifier or create a new one
        res, created = models.Identifier.objects.get_or_create(type=id_type, name=name)
        return res

    def get_contribution_type(self, name):
        res = self.types.get(name, None)
        if res is None:
            self.results.error(404, "Contribution type '%s' does not exist in source %s" % (name, self.source.name))
        return res

    def get_contribution(self, identifier, data):
        """
        Validate and insert or update a contribution, extending its span if it
        already exists
        """
        data = ValidatingDict(self.results, data, "contribution JSON data")
        cnew = models.Contribution(
            identifier=identifier,
            type=self.get_contribution_type(data.get_string("type")),
            begin=data.get_date_or_none("begin"),
            until=data.get_date_or_none("end"),
            url=data.get_unicode("url", empty=True) or None,
        )

        try:
            # Look for an existing contribution
            c = models.Contribution.objects.get(identifier=identifier, type=cnew.type)
        except models.Contribution.DoesNotExist:
            cnew.save()
            self.results.contributions_created += 1
            return cnew

        changed = c.extend(cnew.begin, cnew.until)

        if c.url != cnew.url:
            c.url = cnew.url
            changed = True

        if changed:
            self.results.contributions_updated += 1
            c.save()

        return c

    def import_datum(self, data):
        """
        Import a list of contributions
        """
        try:
            data = ValidatingDict(self.results, data, "source JSON data")

            # Get the identifier list
            src_ids = data.get_sequence("id")

            # Get the contributions list
            src_contribs = data.get_sequence("contributions")

            # Decode identifiers
            identifiers = []
            for i in src_ids:
                try:
                    identifiers.append(self.get_identifier(i))
                except Fail:
                    self.results.identifiers_skipped += 1
            if not identifiers:
                self.results.error(404, "no identities found in contribution record")

            # Decode contributions
            contribs = []
            for i in identifiers:
                for sc in src_contribs:
                    self.results.contributions_processed += 1
                    try:
                        contribs.append(self.get_contribution(i, sc))
                    except Fail:
                        self.results.contributions_skipped += 1
            if not contribs:
                log.warning("No contribution found from %s" % self.source.name)

            return True
        except Fail:
            return False

    def import_data(self, data):
        """
        Import a list of contributions

        Returns True if some data was imported, or if data is the empty list.
        """
        try:
            if not isinstance(data, (list, tuple)):
                self.results.error(400, "Source data is not a list")
        except Fail:
            return False

        if not data:
            return True

        res = False
        for d in data:
            self.results.records_processed += 1
            if self.import_datum(d):
                self.results.records_imported += 1
                res = True
            else:
                self.results.records_skipped += 1
        return res


class Importer(object):
    """
    Data import infrastructure

    Methods starting with import_* return True if something was imported, else
    False.

    Other methods return what they should, or raise Fail if some validation failed.
    """
    def __init__(self):
        self.results = Results()

    def get_source(self, name):
        """
        Get the importer.Source object for a Source with this name
        """
        try:
            source = models.Source.objects.get(name=name)
        except models.Source.DoesNotExist:
            self.results.error(404, "source '%s' not found" % name)
        return Source(self.results, source)

    def import_post(self, postdata):
        """
        Validate and import data from a request.POST dictionary
        """
        try:
            data = ValidatingDict(self.results, postdata, "POST data", strict_dict=False)

            # Validate the source
            source = self.get_source(data.get_string("source"))

            # Validate the auth token
            source.validate_auth_token(data.get_string("auth_token"))

            # Validate the data
            data = data.get_json("data")

            # Import the data
            return source.import_data(data)
        except Fail:
            return False

    def import_request(self, request):
        """
        Validate and import data from a request
        """
        try:
            # We only support POST
            if request.method != "POST":
                self.results.error(400, "only POST requests are accepted")
            return self.import_post(request.POST)
        except Fail:
            return False

class SourceImporter(object):
    """
    Source description import infrastructure

    Methods starting with import_* return True if something was imported, else
    False.

    Other methods return what they should, or raise Fail if some validation failed.
    """
    def __init__(self):
        self.results = Results()

    def get_contribution_type(self, source, data):
        data = ValidatingDict(self.results, data, "contribution type data")

        defaults={
            "desc": data.get_unicode("desc"),
            "contrib_desc": data.get_unicode("contrib_desc"),
        }

        ct, created = models.ContributionType.objects.get_or_create(
            source=source,
            name=data.get_string("name"),
            defaults=defaults)
        if not created:
            ct.desc = defaults["desc"]
            ct.contrib_desc = defaults["contrib_desc"]
            ct.save()

        return ct

    def import_source(self, data):
        """
        Validate and import one source from a dict
        """
        try:
            data = ValidatingDict(self.results, data, "source data")

            defaults={
                "desc": data.get_unicode("desc"),
                "url": data.get_unicode("url"),
                "auth_token": data.get_string("auth_token"),
            }

            ctypes = data.get_sequence("contribution_types")

            s, created = models.Source.objects.get_or_create(
                name=data.get_string("name"),
                defaults=defaults)
            if not created:
                s.desc = defaults["desc"]
                s.url = defaults["url"]
                s.auth_token = defaults["auth_token"]
                s.save()

            old_ctypes = { ct.pk: ct for ct in s.contribution_types.all() }

            for c in ctypes:
                ct = self.get_contribution_type(s, c)
                old_ctypes.pop(ct.pk, None)

            # Delete old ones
            for ct in old_ctypes.iteritems():
                ct.delete()

            return True
        except Fail:
            return False

    def import_sources(self, data):
        """
        Validate and import sources from a list of exported sources
        """
        try:
            if not isinstance(data, (list, tuple)):
                self.results.error(400, "Sources data is not a list")

            res = not data
            for d in data:
                res = self.import_source(d) or res
            return res

        except Fail:
            return False
