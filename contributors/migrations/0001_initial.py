# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'UserProfile'
        db.create_table(u'contributors_userprofile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
        ))
        db.send_create_signal(u'contributors', ['UserProfile'])

        # Adding model 'Identifier'
        db.create_table(u'contributors_identifier', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=16)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='identifiers', null=True, to=orm['auth.User'])),
            ('hidden', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'contributors', ['Identifier'])

        # Adding unique constraint on 'Identifier', fields ['type', 'name']
        db.create_unique(u'contributors_identifier', ['type', 'name'])

        # Adding model 'Source'
        db.create_table(u'contributors_source', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32)),
            ('desc', self.gf('django.db.models.fields.TextField')()),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=255, null=True, blank=True)),
            ('auth_token', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'contributors', ['Source'])

        # Adding model 'ContributionType'
        db.create_table(u'contributors_contributiontype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('source', self.gf('django.db.models.fields.related.ForeignKey')(related_name='contribution_types', to=orm['contributors.Source'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('desc', self.gf('django.db.models.fields.TextField')()),
            ('contrib_desc', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'contributors', ['ContributionType'])

        # Adding unique constraint on 'ContributionType', fields ['source', 'name']
        db.create_unique(u'contributors_contributiontype', ['source_id', 'name'])

        # Adding model 'ContributionTypeSettings'
        db.create_table(u'contributors_contributiontypesettings', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('identifier', self.gf('django.db.models.fields.related.ForeignKey')(related_name='type_settings', to=orm['contributors.Identifier'])),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='identifier_settings', to=orm['contributors.ContributionType'])),
            ('hidden', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'contributors', ['ContributionTypeSettings'])

        # Adding unique constraint on 'ContributionTypeSettings', fields ['identifier', 'type']
        db.create_unique(u'contributors_contributiontypesettings', ['identifier_id', 'type_id'])

        # Adding model 'Contributions'
        db.create_table(u'contributors_contributions', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='contributions', to=orm['contributors.ContributionType'])),
            ('identifier', self.gf('django.db.models.fields.related.ForeignKey')(related_name='contributions', to=orm['contributors.Identifier'])),
            ('begin', self.gf('django.db.models.fields.DateField')()),
            ('until', self.gf('django.db.models.fields.DateField')()),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=255, null=True, blank=True)),
        ))
        db.send_create_signal(u'contributors', ['Contributions'])

        # Adding unique constraint on 'Contributions', fields ['identifier', 'type']
        db.create_unique(u'contributors_contributions', ['identifier_id', 'type_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'Contributions', fields ['identifier', 'type']
        db.delete_unique(u'contributors_contributions', ['identifier_id', 'type_id'])

        # Removing unique constraint on 'ContributionTypeSettings', fields ['identifier', 'type']
        db.delete_unique(u'contributors_contributiontypesettings', ['identifier_id', 'type_id'])

        # Removing unique constraint on 'ContributionType', fields ['source', 'name']
        db.delete_unique(u'contributors_contributiontype', ['source_id', 'name'])

        # Removing unique constraint on 'Identifier', fields ['type', 'name']
        db.delete_unique(u'contributors_identifier', ['type', 'name'])

        # Deleting model 'UserProfile'
        db.delete_table(u'contributors_userprofile')

        # Deleting model 'Identifier'
        db.delete_table(u'contributors_identifier')

        # Deleting model 'Source'
        db.delete_table(u'contributors_source')

        # Deleting model 'ContributionType'
        db.delete_table(u'contributors_contributiontype')

        # Deleting model 'ContributionTypeSettings'
        db.delete_table(u'contributors_contributiontypesettings')

        # Deleting model 'Contributions'
        db.delete_table(u'contributors_contributions')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'contributors.contributions': {
            'Meta': {'unique_together': "(('identifier', 'type'),)", 'object_name': 'Contributions'},
            'begin': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifier': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'contributions'", 'to': u"orm['contributors.Identifier']"}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'contributions'", 'to': u"orm['contributors.ContributionType']"}),
            'until': ('django.db.models.fields.DateField', [], {}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'contributors.contributiontype': {
            'Meta': {'unique_together': "(('source', 'name'),)", 'object_name': 'ContributionType'},
            'contrib_desc': ('django.db.models.fields.TextField', [], {}),
            'desc': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'source': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'contribution_types'", 'to': u"orm['contributors.Source']"})
        },
        u'contributors.contributiontypesettings': {
            'Meta': {'unique_together': "(('identifier', 'type'),)", 'object_name': 'ContributionTypeSettings'},
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifier': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'type_settings'", 'to': u"orm['contributors.Identifier']"}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'identifier_settings'", 'to': u"orm['contributors.ContributionType']"})
        },
        u'contributors.identifier': {
            'Meta': {'unique_together': "(('type', 'name'),)", 'object_name': 'Identifier'},
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '16'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'identifiers'", 'null': 'True', 'to': u"orm['auth.User']"})
        },
        u'contributors.source': {
            'Meta': {'object_name': 'Source'},
            'auth_token': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'desc': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'contributors.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        }
    }

    complete_apps = ['contributors']